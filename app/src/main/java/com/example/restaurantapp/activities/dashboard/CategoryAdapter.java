package com.example.restaurantapp.activities.dashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.databinding.RecyclerCategoryListBinding;

import org.json.JSONArray;
import org.json.JSONObject;

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private JSONArray jsonArray;
    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    public static final String TAG = CategoryAdapter.class.getSimpleName();

    public interface OnItemClickListener {
        void onItemClick(JSONObject obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public CategoryAdapter(Context context) {
        ctx = context;
        jsonArray = new JSONArray();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerCategoryListBinding recyclerCategoryListBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_category_list, parent, false);

        return new MyViewHolder(recyclerCategoryListBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            try {
                MyViewHolder view = (MyViewHolder) holder;
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                String drawable = jsonObject.getString("category");
                view.recyclerCategoryListBinding.tabCategory.setText(drawable);
                if (drawable.equalsIgnoreCase("Special Menu")){
                    view.recyclerCategoryListBinding.categoryImg.setImageDrawable(ctx.getResources().getDrawable(R.drawable.special_menu));
                }else {
                Utils.displayImageOriginalString(ctx, view.recyclerCategoryListBinding.categoryImg, jsonObject.getString("category_url"));}
                if (((DashBoardActivity) ctx).getRestaurentStatus().equalsIgnoreCase("closed")) {
                    //   view.lyt_parent.setBackgroundColor(context.getResources().getColor(R.color.grey_20));
                    view.recyclerCategoryListBinding.rlCategory.setAlpha(Float.valueOf((float) 0.3));
                }
                view.recyclerCategoryListBinding.rlCategory.setOnClickListener(v -> {
                    mOnItemClickListener.onItemClick(jsonObject, position);
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int getItemCount() {
        return jsonArray.length();
    }

    public void setData(JSONArray jsonArray) {
        try {
            this.jsonArray.put(new JSONObject().put("category_id", 0)
                    .put("category", "Special Menu")
                    .put("restaurant_id", ctx.getResources().getString(R.string.restaurant))
                    .put("category_url", "https://storage.googleapis.com/restaurant60-dev-media/Veg_Appetizer.jpg")  //dummy
                    .put("image", "null"));

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("category_id", jsonArray.getJSONObject(i).getString("category_id"))
                        .put("category", jsonArray.getJSONObject(i).getString("category"))
                        .put("restaurant_id", jsonArray.getJSONObject(i).getString("restaurant_id"))
                        .put("category_url", jsonArray.getJSONObject(i).getString("category_url"))
                        .put("image", jsonArray.getJSONObject(i).getString("image"));
                this.jsonArray.put(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerCategoryListBinding recyclerCategoryListBinding;

        public MyViewHolder(@NonNull RecyclerCategoryListBinding recyclerCategoryListBinding) {
            super(recyclerCategoryListBinding.getRoot());
            this.recyclerCategoryListBinding = recyclerCategoryListBinding;
        }
    }
}
