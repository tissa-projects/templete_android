package com.example.restaurantapp.activities.multilocation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.restaurantapp.R;
import com.example.restaurantapp.activities.dashboard.DashBoardActivity;

public class MultiLocation extends AppCompatActivity {
Button xyz;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_location);
        xyz=findViewById(R.id.xyz);
        xyz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MultiLocation.this, DashBoardActivity.class));
            }
        });
    }
}