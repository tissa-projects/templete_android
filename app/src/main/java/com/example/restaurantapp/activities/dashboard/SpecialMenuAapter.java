package com.example.restaurantapp.activities.dashboard;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.example.restaurantapp.R;
import com.example.restaurantapp.SliderItem;
import com.example.restaurantapp.Utilities.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class SpecialMenuAapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

   // private List<SliderItem> sliderItemList;
    private JSONArray jsonArray;
    private ViewPager2 viewPager2;
    private Context context;

    public SpecialMenuAapter(Context context, ViewPager2 viewPager2) {
        jsonArray = new JSONArray();
       // this.sliderItemList = sliderItemList;
        this.viewPager2 = viewPager2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SliderViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.slide_item_container, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {

            if (holder instanceof SliderViewHolder) {
                SliderViewHolder myHolder = (SliderViewHolder) holder;
              //  myHolder.setImage(sliderItemList.get(position));
                myHolder.setViewPaerData(jsonArray.getJSONObject(position));

                if (position == jsonArray.length() - 2) {
                    viewPager2.post(runnable);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
       // return sliderItemList.size();
        return jsonArray.length();
    }

    public void setData(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }

    class SliderViewHolder extends RecyclerView.ViewHolder {

       // private RoundedImageView imageView;
        private ImageView imageView;
        private TextView menuName,menuPrice;

        public SliderViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageSlide);
            menuName = itemView.findViewById(R.id.menu_name);
            menuPrice = itemView.findViewById(R.id.menu_price);
        }

        void setImage(SliderItem sliderItem) {
            imageView.setImageResource(sliderItem.getImage());
        }
        void setViewPaerData(JSONObject jsonObject) {
            try {
                Uri myUri = Uri.parse(jsonObject.getString("product_url"));
                Log.e("TAG", "setViewPaerData: "+myUri );
              // imageView.setImageURI(myUri);
                Utils.displayImageOriginalString(context,imageView,jsonObject.getString("product_url"));
                menuName.setText(jsonObject.getString("product_name"));
                menuPrice.setText("$"+jsonObject.getString("price"));
            }catch (Exception e){e.printStackTrace();}
        }

    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            jsonArray.put(jsonArray);
            notifyDataSetChanged();
        }
    };
}
