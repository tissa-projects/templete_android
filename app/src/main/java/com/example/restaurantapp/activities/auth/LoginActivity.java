package com.example.restaurantapp.activities.auth;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.Preferences;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.UserSession;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.activities.dashboard.DashBoardActivity;
import com.example.restaurantapp.activities.multilocation.MultiLocation;
import com.example.restaurantapp.databinding.ActivityLoginBinding;
import com.example.restaurantapp.interfaces.clickinterfaces.SetOnClickListener;

import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements SetOnClickListener, View.OnClickListener {

    private ActivityLoginBinding binding;
    private AuthViewModel viewModel;
    private Context context;
    private String currentVersion, latestVersion;
    private Dialog dialog;
    public static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = ViewModelProviders.of(this).get(AuthViewModel.class);
        context = LoginActivity.this;
        init();
    }

    private void init() {
        binding.forgotPassword.setOnClickListener(this);
        binding.forgotUser.setOnClickListener(this);
        binding.btnGuest.setOnClickListener(v -> {
            startActivity(new Intent(context, MobileVerificationActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });
        binding.btnLogin.setOnClickListener(v -> {
            if (viewModel.validateLogin(context, binding)) {
                if (VU.isConnectingToInternet(context)) {
                    checkForLogin();
                }
            }
        });

        binding.signUp.setOnClickListener(v -> {
            startActivity(new Intent(context, SignUpActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        });
    }

    private void checkForLogin() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.login);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("username", binding.edtEmail.getText().toString());
            stringMap.put("password", binding.edtPassword.getText().toString());
            stringMap.put("restaurant_id", "1");
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.login(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "checkForLogin: " + jsonObject.toString());
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.AUTHORIZATION_TOKEN, jsonObject.getString("token"));
                        Preferences.setToken(jsonObject.getString("token"));
                        UserSession.setIsuserLogin(context);
                        UserSession.setuserId(context, jsonObject.getString("id"));
                        Preferences.setUserId(jsonObject.getString("id"));
                        Intent intent = new Intent(context, DashBoardActivity.class);
                        Preferences.setUserProfile("1");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        Utils.fadeAnimation(context);

                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401)
                        Utils.getToast(context, getResources().getString(R.string.credential_wrong));
                    else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 403)
                        Utils.getToast(context, getResources().getString(R.string.email_not_verified));
                    else
                        Utils.getToast(context, getResources().getString(R.string.no_response));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    protected void ForgotPassowrd(String username) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.forgot_password_api);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("username", username);
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.forgotPasword(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "ForgotPassowrd: " + jsonObject.toString());
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        Utils.getToast(context, getResources().getString(R.string.password_snt));
                    } else {
                        CustomDialogs.dialogShowMsg(context, jsonObject.getString("msg"));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    protected void ForgotUser(String email) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.forgot_user_api);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("email", email);
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.forgotPasword(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "ForgotUser: " + jsonObject.toString());
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        Utils.getToast(context, getResources().getString(R.string.username_snt));
                    } else {
                        CustomDialogs.dialogShowMsg(context, jsonObject.getString("msg"));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_up:
                startActivity(new Intent(context, SignUpActivity.class));
                Utils.fadeAnimation(context);
                break;
            case R.id.forgot_password:
                viewModel.dialogForgotPasswordUser("password", context);
                break;
            case R.id.forgot_user:
                viewModel.dialogForgotPasswordUser("user", context);
                break;
        }
    }

    // login btn Click
    @Override
    public void onCLick() {
        if (viewModel.validateLogin(context, binding)) {
            if (VU.isConnectingToInternet(context)) {
                checkForLogin();
            }
        }
    }
}
