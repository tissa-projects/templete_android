package com.example.restaurantapp.activities.Address;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.databinding.ActivityBillingAddressBinding;


public class AddressViewModel extends AndroidViewModel {
    private AddressRepository repository;

    public AddressViewModel(@NonNull Application application) {
        super(application);

        repository = AddressRepository.getInstance(application);
    }

    public MutableLiveData<String> getBillingAddress(RestAPIClientHelper helper) {
        return repository.getBillingAddress(helper);
    }

    public MutableLiveData<String> setBillingAddress(RestAPIClientHelper helper) {
        return repository.setBillingAddress(helper);
    }

    public MutableLiveData<String> getStateList(RestAPIClientHelper helper) {
        return repository.getStateList(helper);
    }

    public boolean biillingValidate(Context context, ActivityBillingAddressBinding addressBinding) {
        if (!addressBinding.checkboxCurrentAddress.isChecked()) {
            if (addressBinding.billingEdtName.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.name));
                return false;
            } else if (addressBinding.billingEdtAddressLine.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.address));
                return false;
            }
//            else if (addressBinding.billingEdtHouseNo.getText().toString().equalsIgnoreCase("")) {
//                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.house_no));
//                return false;
//            }
            else if (addressBinding.billingEdtZipCode.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.zip_postal));
                return false;
            } else if (addressBinding.billingEdtCity.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.city));
                return false;
            } else if (addressBinding.billingEdtState.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.state));
                return false;
            } else if (addressBinding.billingEdtCountry.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.country));
                return false;
            }
        }
        return true;
    }
}
