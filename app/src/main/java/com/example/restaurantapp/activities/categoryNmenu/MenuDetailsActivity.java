package com.example.restaurantapp.activities.categoryNmenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.restaurantapp.R;
import com.example.restaurantapp.Utilities.CustomDialogs;
import com.example.restaurantapp.Utilities.SharePreferenceUtil;
import com.example.restaurantapp.Utilities.UserSession;
import com.example.restaurantapp.Utilities.Utils;
import com.example.restaurantapp.Utilities.VU;
import com.example.restaurantapp.activities.ServiceCall.RestAPIClientHelper;
import com.example.restaurantapp.databinding.ActivityMenuDetailsBinding;
import com.example.restaurantapp.interfaces.clickinterfaces.SetOnClickListener;
import com.example.restaurantapp.models.SelectedIngredentModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MenuDetailsActivity extends AppCompatActivity implements SetOnClickListener {

    private ActivityMenuDetailsBinding binding;
    private MenuViewModel viewModel;
    private IngredientAdapter ingredientAdapter;
    private Context context;
    private JSONArray addToCartReqArray;
    private Map<Integer, SelectedIngredentModel> selectedIngredient;
    private JSONObject jsonIntentData;
    private String currencyType;
    public static final String TAG = MenuDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_menu_details);
        viewModel = ViewModelProviders.of(this).get(MenuViewModel.class);
        context = MenuDetailsActivity.this;
        addToCartReqArray = new JSONArray();
        selectedIngredient = new HashMap<>();
        currencyType = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE);
        getIntentData();
        init();

        initIngredientRecycler();
        if (VU.isConnectingToInternet(context)) {
            getCart();
        }
        if (VU.isConnectingToInternet(context)) {
            getIngredientList();
        }
        calculateSubTotalOverQty();

    }

    JSONObject selectedIngredentObj = null;
    private void initIngredientRecycler() {
        ingredientAdapter = new IngredientAdapter(context);
        binding.setIngredientAdapter(ingredientAdapter);

        // on item list clicked
        ingredientAdapter.setOnItemClickListener((JSONObject obj, int position, boolean ischecked, String qty, double price, IngredientAdapter.MyViewHolder myViewHolder) -> {
            //     startActivity(new Intent(context, MenuDetailsActivity.class).putExtra("data", obj.toString()));
            //  Utils.fadeAnimation(context);
            Log.e(TAG, "initIngredientRecycler: ischecked : " + ischecked);

            if (ischecked) {

                selectedIngredentObj = obj;
                Log.e(TAG, "initIngredientRecycler: price: " + price);
                selectedIngredient.put(position, new SelectedIngredentModel(obj, ischecked, qty, price));
                binding.txtSubtotal.setText("$" + Utils.convertToUSDFormat(Double.valueOf(binding.txtSubtotal.getText().toString().replace("$", "")) +
                        (Double.valueOf(myViewHolder.recyclerProductIngredientBinding.price.getText().toString().replace("$", "")))+""));
            } else {
                if (selectedIngredient.containsKey(position)) {
                    selectedIngredient.remove(position);
                    binding.txtSubtotal.setText("$" + Utils.convertToUSDFormat(Double.valueOf(binding.txtSubtotal.getText().toString().replace("$", "")) -
                            (Double.valueOf(myViewHolder.recyclerProductIngredientBinding.price.getText().toString().replace("$", "")))+""));
                }
            }
            //   ingredientAdapter.notifyDataSetChanged();
        });

        ingredientAdapter.setOnIngredientQtyListener(new IngredientAdapter.OnvalueChangeListener() {
            @Override
            public void onItemClick(IngredientAdapter.MyViewHolder view1, String price, CheckBox checkBox, int oldValue, int newValue,int position) {
                if (checkBox.isChecked()) {
                    if (selectedIngredient.containsKey(position)) {
                        selectedIngredient.put(position, new SelectedIngredentModel(selectedIngredentObj, true,
                                view1.recyclerProductIngredientBinding.txtQty.getNumber(),Double.valueOf(price)));
                    }
                    if (newValue > oldValue) {
                        binding.txtSubtotal.setText("$" + Utils.convertToUSDFormat((Double.valueOf(binding.txtSubtotal.getText().toString().replace("$", "")) +
                                (Double.valueOf(price)) + "")));
                    } else {
                        binding.txtSubtotal.setText("$" + Utils.convertToUSDFormat((Double.valueOf(binding.txtSubtotal.getText().toString().replace("$", "")) -
                                (Double.valueOf(price)) + "")));
                    }
                    String newPrice = Utils.convertToUSDFormat((Double.valueOf(price)
                            * newValue) + "");
                    view1.recyclerProductIngredientBinding.price.setText(SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE) + Utils.convertToUSDFormat(newPrice + ""));
                } else {
                    Utils.getToast(context, context.getResources().getString(R.string.checkbox_select_msg));
                    view1.recyclerProductIngredientBinding.txtQty.setNumber(oldValue + "");
                }

            }
        });
    }

    private void calculateSubTotalOverQty() {
        binding.txtQty.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                Log.d(TAG, String.format("oldValue: %d   newValue: %d", oldValue, newValue));

            //    float unitCost = Float.valueOf(binding.txtUnitPrice.getText().toString().replace(",", "").split("\\$")[1]);
             //   float qty = Float.valueOf(binding.txtQty.getNumber());
                if (newValue > oldValue) {
                    binding.txtSubtotal.setText(currencyType +
                            Utils.convertToUSDFormat(((Double.valueOf(binding.txtSubtotal.getText().toString().replace("$", ""))
                                    + (Double.valueOf(binding.txtUnitPrice.getText().toString().replace("$", "")))) + "")));
                } else {
                    binding.txtSubtotal.setText(currencyType +
                            Utils.convertToUSDFormat(((Double.valueOf(binding.txtSubtotal.getText().toString().replace("$", ""))
                                    - (Double.valueOf(binding.txtUnitPrice.getText().toString().replace("$", "")))) + "")));
                }
            }
        });
    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                String data = bundle.getString("data");
                jsonIntentData = new JSONObject(data);
                Log.e(TAG, "getIntentData: " + jsonIntentData.toString());
                String productName = jsonIntentData.getString("productName");
                String productUrl = jsonIntentData.getString("productUrl");
                binding.productName.setText(productName);
                binding.txtUnitPrice.setText(currencyType + Utils.convertToUSDFormat(jsonIntentData.getString("price")));
                binding.txtSubtotal.setText(currencyType + Utils.convertToUSDFormat(jsonIntentData.getString("price")));
                Utils.displayImageOriginalString(context, binding.productImage, productUrl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Menu Details");
        findViewById(R.id.title_bar_left_arrow).setVisibility(View.VISIBLE);
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());
        binding.setAddToCartBtnClcik(this::onCLick);

    }

    @Override // add to cart_app_icon btn click
    public void onCLick() {
        if (VU.isConnectingToInternet(context))
            addToCart();
    }

    private void addToCart() {
        Log.e(TAG, "addToCart: " + binding.txtQty.getNumber());
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            String prooductId = jsonIntentData.getString("productId");
            addToCartReqArray.put(new JSONObject()
                    .put("updated_at", Utils.CurrentTimeStamp())
                    .put("extra", binding.edtSpclInstruction.getText().toString())
                    .put("quantity", binding.txtQty.getNumber())
                    .put("cart_id", SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID))
                    .put("product_id", prooductId)
                    .put("ingredient_id", 0)
                    .put("sequence_id", SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT)));
            //    .put("sequence_id", 0));

            for (int i = 0; i < selectedIngredient.size(); i++) {
                addToCartReqArray.put(new JSONObject()
                        .put("updated_at", Utils.CurrentTimeStamp())
                        .put("extra", "")
                        .put("quantity", selectedIngredient.get(i).getQty())
                        .put("cart_id", SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID))
                        .put("product_id", prooductId)
                        .put("ingredient_id", selectedIngredient.get(i).getJsonObject().getString("ingredient_id"))
                        .put("sequence_id", SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT)));
                //  .put("sequence_id", i+1));
            }
            Log.e(TAG, "addToCart: " + addToCartReqArray.toString());
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.add_to_cart));
            helper.setUrlParameter(addToCartReqArray.toString());
        } catch (Exception e) {
            e.printStackTrace();

        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getAddToCartData(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "addToCart : response: " + responseObj);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 201) {
                        CustomDialogs.dialogCustomShowMsg(MenuDetailsActivity.class, context, getResources().getString(R.string.product_added_into_cart)).findViewById(R.id.btn_ok).setOnClickListener((View) -> {
                            finish();
                            Utils.fadeAnimation(context);

                        });
                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.getToast(context, getResources().getString(R.string.no_response));
            }
        });
    }

    private void getIngredientList() {

        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            String prooductId = jsonIntentData.getString("productId");

            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_ingredient) + prooductId+"&amp;status=ACTIVE");
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getIngredientList(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getIngredientList: " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONArray responseArray = jsonObject.getJSONArray("results");
                        if (responseArray.length() > 0)
                            binding.txtIngredient.setVisibility(View.VISIBLE);
                        else binding.txtIngredient.setVisibility(View.GONE);
                        Log.e(TAG, "getIngredientList: " + responseArray);
                        ingredientAdapter.setData(responseArray, selectedIngredient);
                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.getToast(context, getResources().getString(R.string.no_response));
            }
        });
    }

    // get cart
    private void getCart() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*if (page == 1) {
            dialogCart = ProgressDialog.show(context, "Please wait", "Loading...");
        }*/
        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.getCart(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 200) {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.e(TAG, "getCart: " + jsonObject);
                        JSONArray resultArray = jsonObject.getJSONArray("results");
                        if (resultArray.length() == 0) {
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CATEGORY_ID_WRT_CART, "");
                            createCart();
                        } else {
                            JSONObject cartObj = resultArray.getJSONObject(0);
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, cartObj.getString("id"));
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CATEGORY_ID_WRT_CART, "restaurant");
                        }
                    } else if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 500) {
                        JSONObject jsonObject = new JSONObject(response);
                        Utils.getToast(context, jsonObject.getString("msg"));  // internal server error
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                Utils.getToast(context, getResources().getString(R.string.no_response));
                e.printStackTrace();
            }
        });
    }

    //create cart
    private void createCart() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        JSONObject reqObj = new JSONObject();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.create_cart_api);
        try {
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("restaurant", getResources().getString(R.string.restaurant));
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProgressDialog dialog = CustomDialogs.showProgressDialog(context);
        viewModel.createCart(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "createCart : " + jsonObject);
                    if (SharePreferenceUtil.getSPintValue(context, SharePreferenceUtil.LAST_RESPONSE_CODE) == 201) {
                        JSONObject resultObj = jsonObject;
                        if (resultObj != null) {
                            //     binding.cartcount.setText("0");
                            Log.e(TAG, "createCart:cart id " + resultObj.getString("id"));
                            Log.e(TAG, "createCart:cart id " + resultObj);
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, resultObj.getString("id"));
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CATEGORY_ID_WRT_CART, resultObj.getString("restaurant"));
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.fadeAnimation(context);
    }
}
