package com.example.restaurantapp.Utilities;

/**
 * Created by JASS-3 on 5/19/2018.
 */

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;


public class Preferences {

    private final static String preferencesName = "The MyPref";//swapnil
    public static Context appContext;



    public static void setUserId(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setUser", val);
        editor.commit();
    }
    public static String getUserId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setUser", null);
        return value;
    }
    public static void setUserProfile(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setUserId", val);
        editor.commit();
    }
    public static String getUserProfile() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setUserId", null);
        return value;
    }

    public static void setToken(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("settoken", val);
        editor.commit();
    }
    public static String getToken() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("settoken", null);
        return value;
    }

    public static void setstatus(int val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putInt("setstatus", val);
        editor.commit();
    }
    public static int getstatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        int value = prefs.getInt("setstatus", 0);
        return value;
    }
}

