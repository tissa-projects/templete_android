package com.example.restaurantapp.Utilities.swa;


import com.example.restaurantapp.models.CartCountRequest;
import com.example.restaurantapp.models.RetrarentRequest;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface API {

    @Headers("Content-Type: application/json")
    @GET("restaurant/1/")
    Call<RetrarentRequest> fetchdata(
            @Header("Authorization") String authorization
    );

    @POST("graphql/")
    Call<CartCountRequest> cartcount(
            @Body RequestBody query
    );
}
